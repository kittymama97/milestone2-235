package controllers;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import beans.User;

@ManagedBean
@ViewScoped
public class LoginController {

	public String onSubmit(User user) {
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("user", user);
		System.out.println(user.getUsername() + " " + user.getPassword());
		
		if (user.getUsername().equals("AndyC") && user.getPassword().equals("CAndy")) {
		return ("loginSuccess.xhtml");
		
		}else {
			return ("loginFailed.xhtml");
		}
	}
}